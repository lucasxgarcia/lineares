#include "ls.h"
#include <stdio.h>
#include <stdlib.h>

/* Aqui devem ser implementadas as funções definidas em ls.h */

//Função Create
struct list * create(int max){
  //Verificação de tamanho para iniciar o struct
  if(max <= 0){
    return NULL;
  }

  struct list* pList;

  pList = malloc(sizeof(struct list));
  pList->arm = malloc((pList->max)*sizeof(elem));	
  pList->max = max;
  pList->tam = 0;
  
  return pList;
}

//Função Insert
int insert(struct list *desc, int pos, elem item){

  if(desc->tam != desc->max){
    desc->arm[pos-1] = item;
    desc->tam++;

    return 1;
  }
  else
    return 0;
}

//Função Delete
int delete(struct list *desc, int pos){
  if(desc->tam != desc->max){
    desc->arm[pos-1] = 0;
    desc->tam--;

    return 1;
  }
  else
    return 0;
}

//Função Get
elem get(struct list *desc, int pos){
  return desc->arm[pos-1];
}

//Função Set
int set(struct list *desc, int pos, elem item){
  if(pos < desc->max){
    desc->arm[pos-1] = item;
    return 1;
  }
  else
    return 0;
}

//Função locate
int locate(struct list *desc, int pos, elem item){
  if(desc->max > pos){
    for(; pos <= desc->max; pos ++ ){
      if(desc->arm[pos-1] == item){
        return pos;
      }
    }
  }
    return 0;
}

//Função Length
int length(struct list *desc){
  return desc->tam;
}

//Função Max
int max(struct list *desc){
  return desc->max;
}

//Função Full
int full(struct list *desc){
  return (desc->max == desc->tam);
    
  
  
}

//Função Destroy
void destroy(struct list *desc){
  free(desc->arm);
  free(desc);
}
